package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

type GetSaleListReq struct {
	g.Meta `path:"/sale/get_list" method:"get" summary:"获取销售列表" tags:"SaleService"`
	Page   int `v:"required#页码不能为空" json:"page" xml:"page" form:"page" header:"page" query:"page"`
	Size   int `v:"required#页大小不能为空" json:"size" xml:"size" form:"size" header:"size" query:"size"`
}

type GetSaleListRes struct {
	g.Meta `path:"/sale/get_list" method:"get" summary:"获取销售列表" tags:"SaleService"`
	List   []saleInfo `json:"list"`
}

type GetSaleReq struct {
	g.Meta `path:"/sale/get" method:"get" summary:"获取销售信息" tags:"SaleService"`
	Id     int64 `v:"required#ID不能为空" json:"id" xml:"id" form:"id" header:"id" query:"id"`
}

type saleInfo struct {
	Id           int64       `json:"id" `
	BuyNumber    int64       `json:"buyNumber"   `
	BuyPrice     float64     `json:"buyPrice"    `
	CategoryId   int64       `json:"categoryId"  `
	MaintainTime *gtime.Time `json:"maintainTime"`
	BuyTime      *gtime.Time `json:"buyTime"     `
}

type GetSaleRes struct {
	g.Meta
	Id           int64       `json:"id" `
	BuyNumber    int64       `json:"buyNumber"   `
	BuyPrice     float64     `json:"buyPrice"    `
	CategoryId   int64       `json:"categoryId"  `
	MaintainTime *gtime.Time `json:"maintainTime"`
	BuyTime      *gtime.Time `json:"buyTime"     `
}

type CreateSaleReq struct {
	g.Meta     `path:"/sale/create" method:"post" summary:"创建销售" tags:"SaleService"`
	BuyTime    *gtime.Time `v:"required#购买时间不能为空" json:"buyTime" xml:"buyTime" form:"buyTime" header:"buyTime" query:"buyTime"`
	BuyNumber  int64       `v:"required#购买数量不能为空" json:"buyNumber" xml:"buyNumber" form:"buyNumber" header:"buyNumber" query:"buyNumber"`
	BuyPrice   float64     `v:"required#购买价格不能为空" json:"buyPrice" xml:"buyPrice" form:"buyPrice" header:"buyPrice" query:"buyPrice"`
	CategoryId int64       `v:"required#分类ID不能为空" json:"categoryId" xml:"categoryId" form:"categoryId" header:"categoryId" query:"categoryId"`
	UserName   string      `v:"required#用户名不能为空" json:"userName" xml:"userName" form:"userName" header:"userName" query:"userName"`
	Contact    string      `v:"required#联系方式不能为空" json:"contact" xml:"contact" form:"contact" header:"contact" query:"contact"`
}

type CreateSaleRes struct {
	g.Meta
	Id int64 `json:"id" `
}

type DeleteSaleReq struct {
	g.Meta `path:"/sale/delete" method:"delete" summary:"删除销售" tags:"SaleService"`
	Id     int64 `v:"required#ID不能为空" json:"id" xml:"id" form:"id" header:"id" query:"id"`
}
type DeleteSaleRes struct {
	g.Meta
}

type UpdateSaleReq struct {
	g.Meta     `path:"/sale/update" method:"put" summary:"更新销售" tags:"SaleService"`
	Id         int64       `v:"required#ID不能为空" json:"id" xml:"id" form:"id" header:"id" query:"id"`
	BuyTime    *gtime.Time `v:"required#购买时间不能为空" json:"buyTime" xml:"buyTime" form:"buyTime" header:"buyTime" query:"buyTime"`
	BuyNumber  int64       `v:"required#购买数量不能为空" json:"buyNumber" xml:"buyNumber" form:"buyNumber" header:"buyNumber" query:"buyNumber"`
	BuyPrice   float64     `v:"required#购买价格不能为空" json:"buyPrice" xml:"buyPrice" form:"buyPrice" header:"buyPrice" query:"buyPrice"`
	CategoryId int64       `v:"required#分类ID不能为空" json:"categoryId" xml:"categoryId" form:"categoryId" header:"categoryId" query:"categoryId"`
	UserName   string      `v:"required#用户名不能为空" json:"userName" xml:"userName" form:"userName" header:"userName" query:"userName"`
	Contact    string      `v:"required#联系方式不能为空" json:"contact" xml:"contact" form:"contact" header:"contact" query:"contact"`
}
type UpdateSaleRes struct {
	g.Meta
}
