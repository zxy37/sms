// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package sale

import (
	"context"

	"sms/api/sale/v1"
)

type ISaleV1 interface {
	GetSaleList(ctx context.Context, req *v1.GetSaleListReq) (res *v1.GetSaleListRes, err error)
	GetSale(ctx context.Context, req *v1.GetSaleReq) (res *v1.GetSaleRes, err error)
	CreateSale(ctx context.Context, req *v1.CreateSaleReq) (res *v1.CreateSaleRes, err error)
	DeleteSale(ctx context.Context, req *v1.DeleteSaleReq) (res *v1.DeleteSaleRes, err error)
	UpdateSale(ctx context.Context, req *v1.UpdateSaleReq) (res *v1.UpdateSaleRes, err error)
}
