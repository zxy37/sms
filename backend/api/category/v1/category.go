package v1

import "github.com/gogf/gf/v2/frame/g"

type category struct {
	Id          int64   `json:"id" v:"required#ID不能为空" gorm:"column:id;primary_key;AUTO_INCREMENT"`
	Name        string  `json:"name" v:"required#名称不能为空" gorm:"column:name"`
	Type        string  `json:"type" v:"required#类型不能为空" gorm:"column:type"`
	Description string  `json:"description" v:"required#描述不能为空" gorm:"column:description"`
	Price       float64 `json:"price" v:"required#价格不能为空" gorm:"column:price"`
	Model       string  `json:"model" v:"required#型号不能为空" gorm:"column:model"`
	CategoryId  int64   `json:"categoryId" v:"required#分类ID不能为空" gorm:"column:category_id"`
}

type CreateCategoryReq struct {
	g.Meta `path:"/sale" method:"post" summary:"增加销售" tags:"CategoryService"`
	category
}
type CreateCategoryRes struct {
	g.Meta
}
type DeleteCategoryReq struct {
	g.Meta `path:"/sale" method:"delete" summary:"删除销售" tags:"CategoryService"`
	Id     int64 `v:"required#id不能为空" json:"id" xml:"id" form:"id" header:"id" query:"id"`
}
type DeleteCategoryRes struct {
	g.Meta
}
type UpdateCategoryReq struct {
	g.Meta `path:"/sale" method:"put" summary:"更新销售" tags:"CategoryService"`
	Id     int64  `v:"required#id不能为空" json:"id" xml:"id" form:"id" header:"id" query:"id"`
	Name   string `v:"required#名称不能为空" json:"name" xml:"name" form:"name" header:"name" query:"name"`
	Type   string `v:"required#类型不能为空" json:"type" xml:"type" form:"type" header:"type" query:"type"`
}

type UpdateCategoryRes struct {
	g.Meta
}
type GetCategoryReq struct {
	g.Meta `path:"/sale" method:"get" summary:"查询销售" tags:"CategoryService"`
	Id     interface{} `v:"required#id不能为空" json:"id" xml:"id" form:"id" header:"id" query:"id"`
}

type GetCategoryRes struct {
	g.Meta
	category
}
type GetCategoryListReq struct {
	g.Meta `path:"/sales" method:"get" summary:"查询所有销售" tags:"CategoryService"`
	Page   int `v:"required#页码不能为空" json:"page" xml:"page" form:"page" header:"page" query:"page"`
	Size   int `v:"required#页码不能为空" json:"size" xml:"size" form:"size" header:"size" query:"size"`
}
type GetCategoryListRes struct {
	g.Meta
	List []category `json:"list"`
}
