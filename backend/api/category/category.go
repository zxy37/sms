// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package category

import (
	"context"

	"sms/api/category/v1"
)

type ICategoryV1 interface {
	CreateCategory(ctx context.Context, req *v1.CreateCategoryReq) (res *v1.CreateCategoryRes, err error)
	DeleteCategory(ctx context.Context, req *v1.DeleteCategoryReq) (res *v1.DeleteCategoryRes, err error)
	UpdateCategory(ctx context.Context, req *v1.UpdateCategoryReq) (res *v1.UpdateCategoryRes, err error)
	GetCategory(ctx context.Context, req *v1.GetCategoryReq) (res *v1.GetCategoryRes, err error)
	GetCategoryList(ctx context.Context, req *v1.GetCategoryListReq) (res *v1.GetCategoryListRes, err error)
}
