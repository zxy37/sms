package v1

import "github.com/gogf/gf/v2/frame/g"

type CreateUserReq struct {
	g.Meta   `path:"/user" method:"post" summary:"create user" tags:"user"`
	Name     string `json:"name" v:"required#name is required" comment:"name"`
	Password string `json:"password" v:"required#password is required" comment:"password"`
	Role     string `json:"role" v:"required#role is required" comment:"role"`
}

type UserInfo struct {
	Id   int64  `json:"id" v:"required#id is required" comment:"id"`
	Name string `json:"name" v:"required#name is required" comment:"name"`
	Role string `json:"role" v:"required#role is required" comment:"role"`
}

type CreateUserRes struct {
	g.Meta
}

type UpdateUserReq struct {
	g.Meta   `path:"/user" method:"put" summary:"update user" tags:"user"`
	Id       int64  `json:"id" v:"required#id is required" comment:"id"`
	Name     string `json:"name" v:"required#name is required" comment:"name"`
	Password string `json:"password" v:"required#password is required" comment:"password"`
}

type UpdateUserRes struct {
	g.Meta
}

type DeleteUserReq struct {
	g.Meta `path:"/user" method:"delete" summary:"delete user" tags:"user"`
	Id     int64 `json:"id" v:"required#id is required" comment:"id"`
}

type DeleteUserRes struct {
	g.Meta
}
type GetUserReq struct {
	g.Meta `path:"/user" method:"get" summary:"get user" tags:"user"`
	Id     int64 `json:"id" v:"required#id is required" comment:"id"`
}
type GetUserRes struct {
	g.Meta
	Id   int64  `json:"id" v:"required#id is required" comment:"id"`
	Name string `json:"name" v:"required#name is required" comment:"name"`
	Role string `json:"role" v:"required#role is required" comment:"role"`
}
type GetUserListReq struct {
	g.Meta `path:"/users" method:"get" summary:"get user list" tags:"user"`
	Page   int `json:"page" v:"required#page is required" comment:"page"`
	Size   int `json:"size" v:"required#size is required" comment:"size"`
}
type GetUserListRes struct {
	g.Meta
	List []*UserInfo
}
