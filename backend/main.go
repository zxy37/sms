package main

import (
	_ "sms/internal/packed"

	_ "sms/internal/logic"

	"github.com/gogf/gf/v2/os/gctx"

	"sms/internal/cmd"
)

func main() {
	cmd.Main.Run(gctx.GetInitCtx())
}
