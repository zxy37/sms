package sale

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/sale/v1"
)

func (c *ControllerV1) UpdateSale(ctx context.Context, req *v1.UpdateSaleReq) (res *v1.UpdateSaleRes, err error) {
	if err = service.Sale().UpdateSale(ctx, &model.SaleUpdateInput{
		Id:         int(req.Id),
		UserName:   req.UserName,
		Contact:    req.Contact,
		BuyTime:    req.BuyTime,
		BuyNumber:  int(req.BuyNumber),
		BuyPrice:   req.BuyPrice,
		CategoryId: int(req.CategoryId),
	}); err != nil {
		return nil, err
	}
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
