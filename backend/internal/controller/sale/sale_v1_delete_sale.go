package sale

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/sale/v1"
)

func (c *ControllerV1) DeleteSale(ctx context.Context, req *v1.DeleteSaleReq) (res *v1.DeleteSaleRes, err error) {
	if err := service.Sale().DeleteSale(ctx, &model.SaleDeleteInput{Id: int(req.Id)}); err != nil {
		return nil, err
	}
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
