package sale

import (
	"context"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/sale/v1"
)

func (c *ControllerV1) GetSaleList(ctx context.Context, req *v1.GetSaleListReq) (res *v1.GetSaleListRes, err error) {

	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
