package sale

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/sale/v1"
)

func (c *ControllerV1) CreateSale(ctx context.Context, req *v1.CreateSaleReq) (res *v1.CreateSaleRes, err error) {
	if err = service.Sale().CreateSale(ctx, &model.SaleCreateInput{
		UserName:   req.UserName,
		Contact:    req.Contact,
		BuyTime:    req.BuyTime,
		BuyNumber:  int(req.BuyNumber),
		BuyPrice:   req.BuyPrice,
		CategoryId: int(req.CategoryId),
	}); err != nil {
		return nil, err
	}
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
