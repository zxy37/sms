package sale

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/sale/v1"
)

func (c *ControllerV1) GetSale(ctx context.Context, req *v1.GetSaleReq) (res *v1.GetSaleRes, err error) {
	sale, err := service.Sale().GetSale(ctx, &model.SaleGetInput{Id: int(req.Id)})
	if err != nil {
		return nil, err
	}
	res = &v1.GetSaleRes{
		Id:         int64(sale.Id),
		BuyPrice:   sale.BuyPrice,
		BuyNumber:  int64(sale.BuyNumber),
		BuyTime:    sale.BuyTime,
		CategoryId: int64(sale.CategoryId),
	}
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
