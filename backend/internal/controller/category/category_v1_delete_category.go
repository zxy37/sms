package category

import (
	"context"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/category/v1"
)

func (c *ControllerV1) DeleteCategory(ctx context.Context, req *v1.DeleteCategoryReq) (res *v1.DeleteCategoryRes, err error) {
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
