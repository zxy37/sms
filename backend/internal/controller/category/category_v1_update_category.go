package category

import (
	"context"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/category/v1"
)

func (c *ControllerV1) UpdateCategory(ctx context.Context, req *v1.UpdateCategoryReq) (res *v1.UpdateCategoryRes, err error) {
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
