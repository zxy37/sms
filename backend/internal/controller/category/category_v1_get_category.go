package category

import (
	"context"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/category/v1"
)

func (c *ControllerV1) GetCategory(ctx context.Context, req *v1.GetCategoryReq) (res *v1.GetCategoryRes, err error) {
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
