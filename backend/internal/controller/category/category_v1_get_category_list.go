package category

import (
	"context"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/category/v1"
)

func (c *ControllerV1) GetCategoryList(ctx context.Context, req *v1.GetCategoryListReq) (res *v1.GetCategoryListRes, err error) {
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
