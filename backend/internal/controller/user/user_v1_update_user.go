package user

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"sms/api/user/v1"
)

func (c *ControllerV1) UpdateUser(ctx context.Context, req *v1.UpdateUserReq) (res *v1.UpdateUserRes, err error) {
	if err := service.User().UpdateUser(ctx, &model.UserUpdateInput{
		Id:       req.Id,
		Password: req.Password,
		UserName: req.Name,
	}); err != nil {
		return nil, err
	}
	return &v1.UpdateUserRes{}, nil
}
