package user

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"sms/api/user/v1"
)

func (c *ControllerV1) DeleteUser(ctx context.Context, req *v1.DeleteUserReq) (res *v1.DeleteUserRes, err error) {
	if err := service.User().DeleteUser(ctx, &model.UserDeleteInput{Id: req.Id}); err != nil {
		return nil, err
	}
	return &v1.DeleteUserRes{}, nil
}
