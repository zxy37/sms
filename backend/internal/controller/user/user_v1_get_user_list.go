package user

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"sms/api/user/v1"
)

func (c *ControllerV1) GetUserList(ctx context.Context, req *v1.GetUserListReq) (res *v1.GetUserListRes, err error) {
	list, err := service.User().GetUserList(ctx, &model.UserGetListInput{Page: req.Page, Size: req.Size})
	if err != nil {
		return nil, err
	}
	for _, v := range list {
		res.List = append(res.List, &v1.UserInfo{
			Id:   v.Id,
			Name: v.UserName,
			Role: v.Role,
		})
	}
	return res, nil
}
