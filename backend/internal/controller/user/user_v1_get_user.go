package user

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"sms/api/user/v1"
)

func (c *ControllerV1) GetUser(ctx context.Context, req *v1.GetUserReq) (res *v1.GetUserRes, err error) {
	if req.Id == 0 {
		return nil, gerror.NewCode(gcode.CodeInvalidParameter, "id is required")
	}
	if user, err := service.User().GetUser(ctx, &model.UserGetInput{Id: req.Id}); err != nil {
		return &v1.GetUserRes{}, err
	} else {
		return &v1.GetUserRes{
			Id:   user.Id,
			Name: user.UserName,
			Role: user.Role,
		}, nil
	}
}
