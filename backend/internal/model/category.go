package model

type CategoryOut struct {
	Model       string  `json:"model"       `
	CategoryId  int     `json:"categoryId"  `
	Id          int     `json:"id"          `
	Name        string  `json:"name"        `
	Type        string  `json:"type"        `
	Description string  `json:"description" `
	Price       float64 `json:"price"       `
}

type CategoryCreateInput struct {
	Model       string  `json:"model"       `
	CategoryId  int     `json:"categoryId"  `
	Name        string  `json:"name"        `
	Type        string  `json:"type"        `
	Description string  `json:"description" `
	Price       float64 `json:"price"       `
}
type CategoryDeleteInput struct {
	Id int `json:"id"          `
}
type CategoryUpdateInput struct {
	Id          int     `json:"id"          `
	Model       string  `json:"model"       `
	CategoryId  int     `json:"categoryId"  `
	Name        string  `json:"name"        `
	Type        string  `json:"type"        `
	Description string  `json:"description" `
	Price       float64 `json:"price"       `
}
type CategoryGetInput struct {
	Id int `json:"id"          `
}

type CategoryGetListInput struct {
	Page int `json:"page"`
	Size int `json:"size"`
}
