package model

import "github.com/gogf/gf/v2/os/gtime"

type SaleOut struct {
	Id           int         `json:"id"`
	UserName     string      `json:"userName"`
	Contact      string      `json:"contact"`
	BuyTime      *gtime.Time `json:"buyTime"`
	BuyNumber    int         `json:"buyNumber"`
	BuyPrice     float64     `json:"buyPrice"`
	CategoryId   int         `json:"categoryId"`
	MaintainTime int         `json:"maintainTime"`
}
type SaleCreateInput struct {
	UserName     string      `json:"userName"`
	Contact      string      `json:"contact"`
	BuyTime      *gtime.Time `json:"buyTime" `
	BuyNumber    int         `json:"buyNumber" `
	BuyPrice     float64     `json:"buyPrice"`
	CategoryId   int         `json:"categoryId" `
	MaintainTime int         `json:"maintainTime"`
}
type SaleDeleteInput struct {
	Id int `json:"id"`
}
type SaleUpdateInput struct {
	Id           int         `json:"id"`
	UserName     string      `json:"userName"`
	Contact      string      `json:"contact"`
	BuyTime      *gtime.Time `json:"buyTime" `
	BuyNumber    int         `json:"buyNumber" `
	BuyPrice     float64     `json:"buyPrice"`
	CategoryId   int         `json:"categoryId" `
	MaintainTime int         `json:"maintainTime"`
}
type SaleGetInput struct {
	Id int `json:"id"`
}
type SaleGetListInput struct {
	Page int `json:"page"`
	Size int `json:"size"`
}
