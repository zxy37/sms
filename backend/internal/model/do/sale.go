// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Sale is the golang structure of table sale for DAO operations like Where/Data.
type Sale struct {
	g.Meta       `orm:"table:sale, do:true"`
	Id           interface{} //
	UserName     interface{} //
	Contact      interface{} //
	BuyTime      *gtime.Time //
	BuyNumber    interface{} //
	BuyPrice     interface{} //
	CategoryId   interface{} //
	MaintainTime interface{} //
}
