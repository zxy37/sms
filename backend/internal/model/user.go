package model

import "github.com/gogf/gf/v2/net/ghttp"

type Context struct {
	Session *ghttp.Session
	User    *ContextUser
}
type ContextUser struct {
	Id       int64
	UserName string
	Role     string
	Password string
}

type UserCreateInput struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
	Role     string `json:"role"`
}
type UserDeleteInput struct {
	Id int64 `json:"id"`
}
type UserUpdateInput struct {
	Id       int64  `json:"id"       `
	UserName string `json:"userName" `
	Password string `json:"password" `
	Role     string `json:"role"     `
}
type UserGetInput struct {
	Id int64 `json:"id"`
}
type UserGetListInput struct {
	Page int `json:"page"`
	Size int `json:"size"`
}
type User struct {
	Id       int64  `json:"id"       `
	UserName string `json:"userName" `
	Password string `json:"password" `
	Role     string `json:"role"     `
}
