// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// Category is the golang structure for table category.
type Category struct {
	Id          int     `json:"id"          ` //
	Name        string  `json:"name"        ` //
	Type        string  `json:"type"        ` //
	Description string  `json:"description" ` //
	Price       float64 `json:"price"       ` //
	Model       string  `json:"model"       ` //
	CategoryId  int     `json:"categoryId"  ` //
}
