// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// User is the golang structure for table user.
type User struct {
	Id       int    `json:"id"       ` //
	UserName string `json:"userName" ` //
	Password string `json:"password" ` //
	Role     string `json:"role"     ` //
}
