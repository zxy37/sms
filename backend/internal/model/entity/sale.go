// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Sale is the golang structure for table sale.
type Sale struct {
	Id           int         `json:"id"           ` //
	UserName     string      `json:"userName"     ` //
	Contact      string      `json:"contact"      ` //
	BuyTime      *gtime.Time `json:"buyTime"      ` //
	BuyNumber    int         `json:"buyNumber"    ` //
	BuyPrice     float64     `json:"buyPrice"     ` //
	CategoryId   int         `json:"categoryId"   ` //
	MaintainTime int         `json:"maintainTime" ` //
}
