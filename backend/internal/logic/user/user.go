package user

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"
)

type sUser struct{}

func init() {
	service.RegisterUser(New())
}

func New() *sUser {
	return &sUser{}
}

func (s *sUser) CreateUser(ctx context.Context, in *model.UserCreateInput) error {

	return nil
}
func (s *sUser) DeleteUser(ctx context.Context, in *model.UserDeleteInput) error {
	return nil
}
func (s *sUser) UpdateUser(ctx context.Context, in *model.UserUpdateInput) error {
	return nil
}
func (s *sUser) GetUser(ctx context.Context, in *model.UserGetInput) (*model.User, error) {
	return nil, nil
}
func (s *sUser) GetUserList(ctx context.Context, in *model.UserGetListInput) ([]*model.User, error) {
	return nil, nil
}
