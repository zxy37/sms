package category

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"
)

type sCategory struct{}

func init() {
	service.RegisterCategory(New())
}

func New() *sCategory {
	return &sCategory{}
}

func (s *sCategory) GetCategory(ctx context.Context, in *model.CategoryGetInput) (out model.CategoryOut, err error) {
	return
}
func (s *sCategory) GetCategoryList(ctx context.Context, in *model.CategoryGetListInput) (out []model.CategoryOut, err error) {
	return
}
func (s *sCategory) CreateCategory(ctx context.Context, in *model.CategoryCreateInput) error {
	return nil
}
func (s *sCategory) UpdateCategory(ctx context.Context, in *model.CategoryUpdateInput) error {
	return nil
}
func (s *sCategory) DeleteCategory(ctx context.Context, in *model.CategoryDeleteInput) error {
	return nil
}
