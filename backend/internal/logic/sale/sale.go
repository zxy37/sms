package sale

import (
	"context"
	"sms/internal/model"
	"sms/internal/service"
)

type sSale struct{}

func init() {
	service.RegisterSale(New())
}
func New() *sSale {
	return &sSale{}
}

func (s *sSale) GetSaleList(ctx context.Context, in *model.SaleGetListInput) (out []model.SaleOut, err error) {

	return nil, nil
}
func (s *sSale) GetSale(ctx context.Context, in *model.SaleGetInput) (out model.SaleOut, err error) {
	return
}
func (s *sSale) DeleteSale(ctx context.Context, in *model.SaleDeleteInput) error {
	return nil
}
func (s *sSale) UpdateSale(ctx context.Context, in *model.SaleUpdateInput) error {
	return nil
}
func (s *sSale) CreateSale(ctx context.Context, in *model.SaleCreateInput) error {
	return nil
}
