// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"sms/internal/model"
)

type (
	ICategory interface {
		GetCategory(ctx context.Context, in *model.CategoryGetInput) (out model.CategoryOut, err error)
		GetCategoryList(ctx context.Context, in *model.CategoryGetListInput) (out []model.CategoryOut, err error)
		CreateCategory(ctx context.Context, in *model.CategoryCreateInput) error
		UpdateCategory(ctx context.Context, in *model.CategoryUpdateInput) error
		DeleteCategory(ctx context.Context, in *model.CategoryDeleteInput) error
	}
)

var (
	localCategory ICategory
)

func Category() ICategory {
	if localCategory == nil {
		panic("implement not found for interface ICategory, forgot register?")
	}
	return localCategory
}

func RegisterCategory(i ICategory) {
	localCategory = i
}
