// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"sms/internal/model"
)

type (
	ISale interface {
		GetSaleList(ctx context.Context, in *model.SaleGetListInput) (out []model.SaleOut, err error)
		GetSale(ctx context.Context, in *model.SaleGetInput) (out model.SaleOut, err error)
		DeleteSale(ctx context.Context, in *model.SaleDeleteInput) error
		UpdateSale(ctx context.Context, in *model.SaleUpdateInput) error
		CreateSale(ctx context.Context, in *model.SaleCreateInput) error
	}
)

var (
	localSale ISale
)

func Sale() ISale {
	if localSale == nil {
		panic("implement not found for interface ISale, forgot register?")
	}
	return localSale
}

func RegisterSale(i ISale) {
	localSale = i
}
