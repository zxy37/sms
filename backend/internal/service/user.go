// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"sms/internal/model"
)

type (
	IUser interface {
		CreateUser(ctx context.Context, in *model.UserCreateInput) error
		DeleteUser(ctx context.Context, in *model.UserDeleteInput) error
		UpdateUser(ctx context.Context, in *model.UserUpdateInput) error
		GetUser(ctx context.Context, in *model.UserGetInput) (*model.User, error)
		GetUserList(ctx context.Context, in *model.UserGetListInput) ([]*model.User, error)
	}
)

var (
	localUser IUser
)

func User() IUser {
	if localUser == nil {
		panic("implement not found for interface IUser, forgot register?")
	}
	return localUser
}

func RegisterUser(i IUser) {
	localUser = i
}
