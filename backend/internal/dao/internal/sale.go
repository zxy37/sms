// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SaleDao is the data access object for table sale.
type SaleDao struct {
	table   string      // table is the underlying table name of the DAO.
	group   string      // group is the database configuration group name of current DAO.
	columns SaleColumns // columns contains all the column names of Table for convenient usage.
}

// SaleColumns defines and stores column names for table sale.
type SaleColumns struct {
	Id           string //
	UserName     string //
	Contact      string //
	BuyTime      string //
	BuyNumber    string //
	BuyPrice     string //
	CategoryId   string //
	MaintainTime string //
}

// saleColumns holds the columns for table sale.
var saleColumns = SaleColumns{
	Id:           "id",
	UserName:     "user_name",
	Contact:      "contact",
	BuyTime:      "buy_time",
	BuyNumber:    "buy_number",
	BuyPrice:     "buy_price",
	CategoryId:   "category_id",
	MaintainTime: "maintain_time",
}

// NewSaleDao creates and returns a new DAO object for table data access.
func NewSaleDao() *SaleDao {
	return &SaleDao{
		group:   "default",
		table:   "sale",
		columns: saleColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SaleDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SaleDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SaleDao) Columns() SaleColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SaleDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SaleDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SaleDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
