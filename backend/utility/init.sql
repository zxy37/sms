# 创建 smd 数据库
CREATE DATABASE IF NOT EXISTS sms;
# 创建品类表、包含品类名称、品类种类、品类描述、品类单价、品类型号、品类 ID
CREATE TABLE IF NOT EXISTS `sms`.`category`
(
    `id`          INT(11)        NOT NULL AUTO_INCREMENT, # 主键
    `name`        VARCHAR(45)    NOT NULL,                # 名称
    `type`        VARCHAR(45)    NOT NULL,                # 种类
    `description` VARCHAR(45)    NOT NULL,                # 描述
    `price`       DECIMAL(10, 2) NOT NULL,                # 单价
    `model`       VARCHAR(45)    NOT NULL,                # 型号
    `category_id` INT(11)        NOT NULL,                # 品类 ID
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB;
# 创建销售记录表、包含 用户名称、联系方式、购买时间、购买数量、购买总价、购买品类 ID 外键为category_id、维护时长
CREATE TABLE IF NOT EXISTS `sms`.`sale`
(
    `id`          INT(11)        NOT NULL AUTO_INCREMENT, # 主键
    `user_name`   VARCHAR(45)    NOT NULL,                # 用户名称
    `contact`     VARCHAR(45)    NOT NULL,                # 联系方式
    `buy_time`    DATETIME       NOT NULL,                # 购买时间
    `buy_number`  INT(11)        NOT NULL,                # 购买数量
    `buy_price`   DECIMAL(10, 2) NOT NULL,                # 购买总价
    `category_id` INT(11)        NOT NULL,                # 品类 ID
    `maintain_time` INT(11)        NOT NULL,                # 维护时长
    PRIMARY KEY (`id`),
    INDEX `fk_sale_category_idx` (`category_id` ASC),
    CONSTRAINT `fk_sale_category`
        FOREIGN KEY (`category_id`)
            REFERENCES `sms`.`category` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
    ENGINE = InnoDB;
# 创建用户表
CREATE TABLE IF NOT EXISTS `sms`.`user`
(
    `id`          INT(11)        NOT NULL AUTO_INCREMENT, # 主键
    `user_name`   VARCHAR(45)    NOT NULL,                # 用户名称
    `password`    VARCHAR(45)    NOT NULL,                # 密码
    `role`        VARCHAR(45)    NOT NULL,                # 权限 admin:管理员、user:普通用户
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB;